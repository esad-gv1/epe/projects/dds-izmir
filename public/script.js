import { loadScripts, loadTextFile } from "./js/utils.js";
import { paginate } from "./js/paginate.js";
import { applyObservers } from "./slide.js";

const elementsToPaginate = [];

//CSS used in this project, including the pagedjs preview css
const styleList = [
    "css/style.css",
    "css/cover.css",
    "css/back.css",
    "vendors/css/paged-preview.css"
];

//additional script to load, not importable as es modules
const scritpList = [
    "vendors/js/markdown-it.js",
    "vendors/js/markdown-it-footnote.js",
    "vendors/js/markdown-it-attrs.js",
    "vendors/js/markdown-it-container.js",
    "vendors/js/markdown-it-span.js",
];

//sync batch loading
await loadScripts(scritpList);

//sync text file loading
const linksTextResponse = await loadTextFile("assets/links.html");
const linksHTML = new DOMParser().parseFromString(linksTextResponse, "text/html");
console.log(linksHTML);

//markdown files to load
const mdFilesList = ["md/cover.md", "md/contenu.md", "md/back.md"];
//const mdScreenContent = "md/contenu.md";
//html elements to be filled from converted md file
const contentScreenDestination = "content";

//markdownit instanciation (old school method as no ES6 modules are available)
const markdownit = window.markdownit
    ({
        // Options for markdownit
        langPrefix: 'language-fr',
        // You can use html markup element
        html: true,
        typographer: true,
        // Replace english quotation by french quotation
        quotes: ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'],
    })
    .use(markdownitContainer) //div
    .use(markdownItAttrs, { //custom html element attributes
        // optional, these are default options
        leftDelimiter: '{',
        rightDelimiter: '}',
        allowedAttributes: [] // empty array = all attributes are allowed
    });

async function layoutHTML() {

    for (let index = 0; index < mdFilesList.length; index++) {

        const mdFile = mdFilesList[index];

        const reponse = await fetch(mdFile);
        const mdContent = await reponse.text();
        //convertion from md to html, returns a string
        const result = markdownit.render(mdContent);
        //console.log("result", result);

        if (mdFile.indexOf("contenu") >= 0) {
            const destinationElement = document.getElementById(contentScreenDestination);
            destinationElement.innerHTML = result;
        }
        //elementsToPaginate.push(destinationElement.cloneNode(true));

    };

}

//window.addEventListener("load", async (event) => {
await layoutHTML();
console.log("layoutHTML");

applyObservers();

//make a hidden HTML part of the document to send the content to pagejs on clicks
const hiddenContent = document.getElementById("hiddenContent");
hiddenContent.innerHTML = linksTextResponse;
const divHiddenContent = [...hiddenContent.getElementsByTagName("div")];

//custom script from here
//const pElements = [...document.getElementsByTagName("p")];
const pElements = [...document.querySelectorAll('.data-div')];

pElements.forEach((p) => {
    p.addEventListener("click", (event) => {
        if (p.dataset.id) {
            console.log("current id : ", p.dataset.id);

            divHiddenContent.forEach((div) => {
                if (div.dataset.id) {
                    if (p.dataset.id === div.dataset.id) {
                        console.log("got a match id : ", p, div);
                        elementsToPaginate.push(div)
                    }
                }
            });

        }
    });
});

window.addEventListener("keydown", (event) => {

    if (event.key === 'i' || event.key === 'I') {
        //make cover and back visible
        const cover = document.getElementById("cover");
        cover.style.display = "block";
        const back = document.getElementById("back");
        back.style.display = "block";

        elementsToPaginate.unshift(cover);
        elementsToPaginate.push(back);
        console.log(elementsToPaginate);
        paginate(elementsToPaginate, styleList);
    }
});
