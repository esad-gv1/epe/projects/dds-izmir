console.log("slide");

//document.addEventListener('DOMContentLoaded', function() {
export function applyObservers() {
    const leftContainers = document.querySelectorAll('.md-text-left');
    const rightContainers = document.querySelectorAll('.md-text-right');
    console.log(leftContainers, rightContainers);

    const observerOptions = {
        root: null,
        threshold: 0.1
    };

    const leftObserverCallback = function (entries, observer) {
        entries.forEach(entry => {

            if (entry.isIntersecting) {
                entry.target.classList.add('md-text-left-slideIn');
                console.log("isIntersecting");
                // } else {
                //     entry.target.classList.remove('md-text-left-slideIn');
                //     console.log("not isIntersecting");
            }
        });
    };

    const leftObserver = new IntersectionObserver(leftObserverCallback, observerOptions);
    leftContainers.forEach(container => {
        leftObserver.observe(container);
    });

    const rightObserverCallback = function (entries, observer) {
        entries.forEach(entry => {

            if (entry.isIntersecting) {
                entry.target.classList.add('md-text-right-slideOut');
                // } else {
                //     entry.target.classList.remove('md-text-right-slideOut');
            }
        });
    };

    const rightObserver = new IntersectionObserver(rightObserverCallback, observerOptions);
    rightContainers.forEach(container => {
        rightObserver.observe(container);
    });
};